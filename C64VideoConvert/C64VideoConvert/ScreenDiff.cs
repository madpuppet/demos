﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace C64VideoConvert
{
    public class ScreenDiff
    {
        public byte[] m_state;
        public byte[] m_other;
        public int m_diffCount;
        public int m_bitDiffCount;

        public delegate int DiffLengthCalc(int idx);
        public delegate void DiffEncoder(BinaryWriter stream, ref int idx, int length);
        public class DiffCallbacks
        {
            public DiffCallbacks(DiffLengthCalc calc, DiffEncoder encoder)
            {
                m_lengthCalc = calc;
                m_encoder = encoder;
            }
            public DiffLengthCalc m_lengthCalc;
            public DiffEncoder m_encoder;
        }

        public List<DiffCallbacks> m_diffs;

        public ScreenDiff()
        {
            m_diffCount = 0;
            m_bitDiffCount = 0;
            m_state = new byte[Screen.WidthInChars / 2 * Screen.HeightInChars / 2];
            m_other = new byte[Screen.WidthInChars / 2 * Screen.HeightInChars / 2];

            m_diffs = new List<DiffCallbacks>();
            m_diffs.Add(new DiffCallbacks(CalcSkipLength, WriteSkip));
            m_diffs.Add(new DiffCallbacks(CalcLiteralLength, WriteLiteral));
            m_diffs.Add(new DiffCallbacks(CalcRepeatLength, WriteRepeat));
        }

        void CopyScreen(Screen screen, byte[] dest)
        {
            for (int y = 0; y < Screen.HeightInChars; y += 2)
            {
                for (int x = 0; x < Screen.WidthInChars; x += 2)
                {
                    int idx = x / 2 + (y * Screen.WidthInChars / 2) / 2;
                    dest[idx] = (byte)((screen.Char(x, y) << 6) + (screen.Char(x + 1, y) << 4) + (screen.Char(x, y + 1) << 2) + screen.Char(x + 1, y + 1));
                }
            }

        }

        public void WriteState(BinaryWriter stream)
        {
            stream.Write(m_state);
        }

        void WriteCmd(BinaryWriter stream, RLEType t, int count)
        {
            byte val = (byte)((int)t + (count << 2));
            //Debug.WriteLine(string.Format("Write Command: {0:x} RLE {1} COUNT {2}", val, t, count));
            stream.Write(val);
        }

        public void WriteSkip(BinaryWriter stream, ref int idx, int length)
        {
            // do single
            int len = Math.Min(63, length - 1);
            WriteCmd(stream, RLEType.Skip, len);
            idx += len + 1;
        }

        public void WriteLiteral(BinaryWriter stream, ref int idx, int length)
        {
            // do single
            int len = Math.Min(63, length - 1);
            WriteCmd(stream, RLEType.Literal, len);
            for (int i = 0; i < len + 1; i++)
            {
                byte outVal = m_other[idx];
                stream.Write(outVal);
                m_state[idx] = outVal;
                idx++;
            }
        }

        public void WriteRepeat(BinaryWriter stream, ref int idx, int length)
        {
            // do single
            int len = Math.Min(63, length - 1);
            WriteCmd(stream, RLEType.Repeat, len);
            byte outVal = m_other[idx];
            stream.Write(outVal);
            for (int i = 0; i < len + 1; i++)
            {
                m_state[idx] = outVal;
                idx++;
            }
        }

        public void WriteDiff(BinaryWriter stream, Screen otherScreen)
        {
            Debug.WriteLine("*** NEW FRAME ***");
            CopyScreen(otherScreen, m_other);

            int idx = 0;
            while (idx < m_state.Length)
            {
                DiffCallbacks best = null;
                int longest = 0;
                foreach (var d in m_diffs)
                {
                    int length = d.m_lengthCalc(idx);
                    if (length > longest)
                    {
                        best = d;
                        longest = length;
                    }
                }
                best.m_encoder(stream, ref idx, longest);
            }
            stream.Write((byte)RLEType.End);
        }

        public int CalcSkipLength(int idx)
        {
            int count = 0;
            while (idx + count < m_state.Length && m_state[idx + count] == m_other[idx + count])
            {
                count++;
            }
            return count;
        }

        public int CalcLiteralLength(int idx)
        {
            int count = 0;
            while (idx + count < m_state.Length && m_state[idx + count] != m_other[idx + count])
            {
                count++;
            }
            return count;
        }

        public int CalcRepeatLength(int idx)
        {
            int count = 1;
            byte v = m_state[idx];
            while (idx + count < m_state.Length && m_state[idx + count] != m_other[idx + count] && v == m_other[idx + count])
            {
                count++;
            }
            return count;
        }
        
    }
}
