﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Diagnostics;

namespace C64VideoConvert
{
    public class Screen
    {
        public static int WidthInChars = 40*2;
        public static int HeightInChars = 25*2;
        public ScreenBlock[] m_blocks;

        public Screen()
        {
            m_blocks = new ScreenBlock[WidthInChars * HeightInChars];
        }

        public byte Char(int x, int y)
        {
            return m_blocks[x + y * WidthInChars].m_char;
        }

        public byte Col(int x, int y)
        {
            return m_blocks[x + y * WidthInChars].m_color;
        }

        public void Set(ScreenBlock blk)
        {
            int idx = blk.m_x + blk.m_y * WidthInChars;
            m_blocks[idx] = blk;
        }

        public void Scan(Bitmap bm)
        {
            for (int y = 0; y < HeightInChars; y++)
            {
                string line = string.Format("{0:D2} : ", y);
                for (int x = 0; x < WidthInChars; x++)
                {
                    var blk = new ScreenBlock(bm, x, y);
                    line += (char)('A' + blk.m_char);
                    Set(blk);
                }
                //Debug.WriteLine(line);
            }
        }

        public void Write(BinaryWriter outStream)
        {
            for (int y = 0; y<HeightInChars; y+=2)
            {
                for (int x = 0; x<WidthInChars; x+=2)
                {
                    byte outChar = (byte)((Char(x, y) << 6) +(Char(x+1, y) << 4) + (Char(x, y+1) << 2) + Char(x+1, y+1));
                    outStream.Write(outChar);
                }
            }
        }
    }
}
