﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;
using System.IO;
using FFMpegCore.FFMPEG;
using FFMpegCore;

namespace C64VideoConvert
{
    enum RLEType
    {
        End,
        Skip,
        Literal,
        Repeat
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Screen> m_screens = new List<Screen>();
            FFMpegOptions.Configure(new FFMpegOptions { RootDirectory = "C:\\Users\\podes\\Documents\\Projects\\Demos\\C64VideoConvert\\packages\\FFMpegCore.1.0.7\\content\\FFMPEG\\bin" });

            double startTime = 9.0; double endTime = 13.0; double frameRate = 15.0; string name = "hitormiss";

            string videoPath = string.Format("{0}.mp4", name);
            var video = VideoInfo.FromPath(videoPath);

            double totalFrames = video.Duration.TotalSeconds * video.FrameRate;
            double frameDuration = 1.0 / video.FrameRate;
            double timePerCapture = 1.0 / frameRate;
            startTime = Math.Min(startTime, video.Duration.TotalSeconds-1.0);
            endTime = Math.Min(video.Duration.TotalSeconds, endTime);

            Console.WriteLine(video.ToString());
            var mpeg = new FFMpeg();
            for (double tm = startTime; tm < endTime; tm += timePerCapture)
            {
                FileInfo outputFile = new FileInfo("_temp12345.png");
                Console.WriteLine("TIME " + tm);
                var bm = mpeg.Snapshot(video, outputFile, new Size(video.Width, video.Height), TimeSpan.FromSeconds(tm));
                var scr = new Screen();
                scr.Scan(bm);
                m_screens.Add(scr);
                bm.Dispose();
            }



            // load all the frames
#if false
            for (int i = startFrame; i < endFrame; i+=frameSkip)
            {
                Console.WriteLine("Tile " + i);
                string path = string.Format("{0}\\{0}{1:D3}.png", name, i);
                Image image = Image.FromFile(path);
                Bitmap bm = new Bitmap(image);
                var scr = new Screen();
                scr.Scan(bm);
                m_screens.Add(scr);
                image.Dispose();
            }
#endif

            // encode..            
            // first page is literal 1000 bytes...
            var screen = new ScreenDiff();
            var outFile = File.Create(string.Format("..\\..\\VideoPlayer\\{0}.dat",name));
            var outStream = new BinaryWriter(outFile);

            // for each other page, write a compressed difference
            foreach (var scr in m_screens)
            {
//                var tempStream = new MemoryStream(16);
//                var tempWriter = new BinaryWriter(tempStream);
//                screen.WriteDiff(tempWriter, scr);

                screen.WriteDiff(outStream, scr);
            }

            outStream.Close();
            outFile.Close();
            outFile.Dispose();
        }

    }
}
