﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace C64VideoConvert
{
    public class ScreenBlock
    {
        // store lots of data for screenblock - mainly for debugging
        // tile on image
        public int m_x;
        public int m_y;
        // average r,g,b
        public float m_r;
        public float m_g;
        public float m_b;
        // average r,g,b converted to intensity and hue
        public float m_intens;
        public float m_hue;
        // c64 out
        public byte m_char;
        public byte m_color;

        public ScreenBlock(Bitmap bm, int x, int y)
        {
            float bmw = (float)bm.Width;
            float bmh = (float)bm.Height;
            float sw = (float)Screen.WidthInChars;
            float sh = (float)Screen.HeightInChars;
            float baspect = bmw / bmh;
            float saspect = sw / sh;
            float xoff, yoff, xscale, yscale;
            if (baspect > saspect)
            {
                // bitmap is wider than c64 screen
                yscale = 1.0f;
                xscale = saspect / baspect;
                xoff = (bmw - bmw * xscale) * 0.5f;
                yoff = 0.0f;
            }
            else
            {
                // bitmap is thinner than the c64 screen
                xscale = 1.0f;
                yscale = baspect / saspect;
                yoff = (bmh - bmh * yscale) * 0.5f;
                xoff = 0.0f;
            }

            // calculate work area
            int x1 = (int)(xoff + (bm.Width * x / Screen.WidthInChars) * xscale);
            int x2 = (int)(xoff + (bm.Width * (x + 1) / Screen.WidthInChars) * xscale);
            int y1 = (int)(yoff + (bm.Height * y / Screen.HeightInChars) * yscale);
            int y2 = (int)(yoff + (bm.Height * (y + 1) / Screen.HeightInChars) * yscale);

            // get average rgb for work area in bitmap
            int r = 0;
            int g = 0;
            int b = 0;
            int count = 0;
            for (int xx=x1; xx<x2; xx++)
            {
                for (int yy = y1; yy < y2; yy++)
                {
                    Color col = bm.GetPixel(xx, yy);
                    r += col.R;
                    g += col.G;
                    b += col.B;
                    count++;
                }
            }

            m_x = x;
            m_y = y;
            m_r = (float)(r / count) / 255.0f;
            m_g = (float)(g / count) / 255.0f;
            m_b = (float)(b / count) / 255.0f;

            m_intens = (m_r + m_g + m_b) / 3.0f;
            m_hue = m_r + m_g + m_b;

            m_char = (byte)(m_intens * 4);
            if (m_char > 3)
                m_char = 3;
            m_color = (byte)(m_hue * 16);
            if (m_color > 15)
                m_color = 15;
        }
    }
}
