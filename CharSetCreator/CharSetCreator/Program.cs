﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CharSetCreator
{
    public class Char
    {
        public byte[] m_data;

        // constructor - creates clear array of 7 bytes
        public Char()
        {
            m_data = new byte[8];
        }

        // set bit using x and y adressing
        public void Set(int x, int y)
        {
            m_data[y] |= (byte)( 1 << x);
        }

        // set bit using 0..64 addressing
        public void Set(int bit)
        {
            if (bit > 63)
                bit = 63;
            m_data[bit / 8] |= (byte)(1 << (bit & 7));
        }
    }

    class Program
    {
        public static byte[] m_char0 = { 0, 0, 0, 0 };
        public static byte[] m_char1 = { 8, 2, 9, 4 };
        public static byte[] m_char2 = { 6, 11, 14, 5 };
        public static byte[] m_char3 = { 15, 15, 15, 15 };
        public static byte[][] m_chars = { m_char0, m_char1, m_char2, m_char3 };

        static void BuildChar(byte[] ch, int tl, int tr, int bl, int br)
        {
            for (int i=0; i<4; i++)
            {
                ch[i] = (byte)((m_chars[tl][i] << 4) | (m_chars[tr][i]));
            }

            for (int i = 0; i < 4; i++)
            {
                ch[i+4] = (byte)((m_chars[bl][i] << 4) | (m_chars[br][i]));
            }
        }

        static void Main(string[] args)
        {
            var file = File.Create("charset.dat");
            var outFile = new BinaryWriter(file);
            byte[] c = new byte[8];
            for (int tl = 0; tl<4; tl++)
            {
                for (int tr = 0; tr<4; tr++)
                {
                    for (int bl = 0; bl < 4; bl++)
                    {
                        for (int br = 0; br < 4; br++)
                        {
                            BuildChar(c, tl, tr, bl, br);

                            for (int b = 0; b < 8; b++)
                                outFile.Write(c[b]);
                        }
                    }
                }
            }

            outFile.Close();
            outFile.Dispose();
        }
    }
}
