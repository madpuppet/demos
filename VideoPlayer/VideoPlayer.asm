#import "../includes/c64.lib"

:BasicUpstart2(start)

.label inptr = $fb
.label outptr = $fd

	* = 2064  "Main"
	
start:
	sei

    // point custom character set to our char set data        
    lda $d018
    and #$f0
    clc
    ora #$0c
    sta $d018
    
    lda #0
    sta $d020
    sta $d021

restart:
    // clear the screen data to the screen     
    lda #0
    ldx #$04
    jsr clearScreen
            
    // now process each frame
    // decompress from inptr -> outptr
    lda #$00
    sta outptr
    sta inptr
    lda #$04
    sta outptr+1
    lda #$38
    sta inptr+1

startPage:    
    jsr pageInAnim
process:
    ldy #0
    lda (inptr),y
    tax
    lda #1
    clc
    adc inptr
    sta inptr
    lda #0
    adc inptr+1
    sta inptr+1
    txa
    and #3
    asl
    tay
    lda rleTable,y
    sta jumper+1
    lda rleTable+1,y
    sta jumper+2
    lda #>(process-1)
    pha
    lda #<(process-1)
    pha
    txa
    lsr
    lsr
    clc
    adc #1

jumper:
    jmp $8000
    
clearScreen:        // a==low  x==hi
    sta out1
    stx out1+1
    clc
    adc #250
    tay
    sta out2
    tax
    tya
    adc #0
    tay
    sta out2+1
    txa
    adc #250
    tax
    sta out3
    tya
    adc #0
    tay
    sta out3+1
    txa
    adc #250
    sta out4
    tya
    adc #0
    sta out4+1
    lda #0
clrloop:
    sta $8000,y
    sta $8000,y
    sta $8000,y
    sta $8000,y
    iny
    cpy #250
    bne clrloop
    rts
    
.label out1 = clrloop+1
.label out2 = clrloop+4
.label out3 = clrloop+7
.label out4 = clrloop+10

rleEnd:
    pla
    pla
    jsr pageOutAnim
    
    lda #$00
    sta outptr
    lda #$04
    sta outptr+1

    lda inptr+1
    cmp #>endOfData
    bne notDone
    lda inptr
    cmp #<endOfData
    bne notDone
    jsr delay
    jmp restart
notDone: 
    jsr delay
    jmp startPage

delay: 
    jsr waitFrame
    jsr waitFrame
    jsr waitFrame
    rts

waitFrame:
    lda #$80
    cmp $d012 
    bne waitFrame
waitFrame2:
    lda #$81
    cmp $d012 
    bne waitFrame2
waitFrameDone:
    rts
    
rleSkip:
    clc
    adc outptr
    sta outptr
    lda #0
    adc outptr+1
    sta outptr+1
    rts

rleLiteral:
    pha
    ldy #0
    tax
loopLiteral:
    lda (inptr),y
    sta (outptr),y
    iny
    dex
    bne loopLiteral
    pla
    tax
    clc
    adc inptr
    sta inptr
    lda #0
    adc inptr+1
    sta inptr+1
    txa
    clc
    adc outptr
    sta outptr
    lda #0
    adc outptr+1
    sta outptr+1
    rts

rleRepeat:
    pha
    tax
    ldy #0
    lda (inptr),y
    
loopRepeat:
    sta (outptr),y
    iny
    dex
    bne loopRepeat
    lda #1
    clc
    adc inptr
    sta inptr
    lda #0
    adc inptr+1
    sta inptr+1
    pla
    clc
    adc outptr
    sta outptr
    lda #0
    adc outptr+1    
    sta outptr+1
    rts    
    
pageInAnim:
    pha
    lda $1
    and #$f8
    sta $1
    pla
    rts
    
pageOutAnim:
    pha
    lda $1
    ora #7
    sta $1
    pla
    rts
    
rleTable:
    .word rleEnd, rleSkip, rleLiteral, rleRepeat

	* = $3000 "Char Data"
	
.import binary "charset.dat"

    * = $3800 "Dancing Data"
    
//.import binary "juggling.dat"
.import binary "hitormiss.dat"
endOfData:

